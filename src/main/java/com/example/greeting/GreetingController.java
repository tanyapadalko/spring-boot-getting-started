package com.example.greeting;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();



    @RequestMapping(method = RequestMethod.GET)
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public ResponseEntity<AdvancedGreeting> advancedGreeting(@PathVariable(name = "name") String name) {
        AdvancedGreeting greeting = new AdvancedGreeting(counter.incrementAndGet(), String.format(template, name), LocalTime.now());
        return new ResponseEntity<>(advanceGreeting(greeting), HttpStatus.OK);
    }

    private AdvancedGreeting advanceGreeting(AdvancedGreeting greeting) {
        String content;
        switch (greeting.getDayPart()) {
            case MORNING:
                content = greeting.getContent().substring(7) + " Have a wonderful morning!";
                return new AdvancedGreeting(greeting.getId(), content,greeting.getDayPart());
            case EVENING:
                content = greeting.getContent().substring(7) + " Have a wonderful evening!";
                return new AdvancedGreeting(greeting.getId(), content,greeting.getDayPart());
            case AFTERNOON:
            default:
                content = greeting.getContent().substring(7) + " Have a wonderful day!";
                return new AdvancedGreeting(greeting.getId(), content,greeting.getDayPart());
        }
    }

}
