package com.example.greeting;

public enum DayPart {

    MORNING, AFTERNOON, EVENING
}
