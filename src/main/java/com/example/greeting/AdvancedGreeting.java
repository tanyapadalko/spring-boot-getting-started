package com.example.greeting;

import java.time.LocalTime;

public class AdvancedGreeting {

    private final long id;
    private final String content;
    private final DayPart dayPart;

    public AdvancedGreeting(long id, String content) {
        this.id = id;
        this.content = content;
        this.dayPart = DayPart.AFTERNOON;
    }

    public AdvancedGreeting(long id, String content, LocalTime currentTime) {
        this.id = id;
        this.content = content;
        this.dayPart = getDayPartFromCurrentTime(currentTime);
    }

    public AdvancedGreeting(long id, String content, DayPart dayPart) {
        this.id = id;
        this.content = content;
        this.dayPart = dayPart;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public DayPart getDayPart() {
        return dayPart;
    }

    private DayPart getDayPartFromCurrentTime(LocalTime currentTime) {
        final LocalTime FIVE_OCLOCK = LocalTime.of(17, 0);

        if(currentTime.isBefore(LocalTime.NOON))
            return DayPart.MORNING;
        if(currentTime.isBefore(FIVE_OCLOCK))
            return DayPart.AFTERNOON;
        if(currentTime.isAfter(FIVE_OCLOCK))
            return DayPart.EVENING;
        return DayPart.AFTERNOON;
    }
}
